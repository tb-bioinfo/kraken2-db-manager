#!/bin/bash

set -e
source settings.conf

info_file=/dev/null
database_built=0

while IFS="=" read -r key value; do
  if [[ -z "$key" || "$key" == \#* ]]; then
    continue
  fi

  key="${key#"${key%%[![:space:]]*}"}"
  key="${key%"${key##*[![:space:]]}"}"
  value="${value#"${value%%[![:space:]]*}"}"
  value="${value%"${value##*[![:space:]]}"}"

  case "$key" in
    db_name)
      db_full_path=${dbs_folder}/`date "+${value}"`
      echo "Create database ${db_full_path}"
      mkdir -p "${db_full_path}/tmp"
      cd "${db_full_path}/tmp"
      info_file="${db_full_path}/database.info"
      echo $(basename "${db_full_path}") >"${info_file}"
      echo "Build date: "`date` >>"${info_file}"
      kraken2-build --version | head -n 1 >>"${info_file}"
      ;;
    description)
      echo "Description: ${value}" >>"${info_file}"
      ;;
    taxonomy_ln)
      if [ "${value}" = '$latest' ]; then
        tax_link="${ncbi_taxonomies_folder}/latest"
      else
        tax_link="${value}"
      fi
      ln -sf "${tax_link}" "${db_full_path}/taxonomy"
      ;;
    fasta_url)
      echo "Download from URL: ${value}"
      wget "${value}"
      file_name="$(basename "${value}")"
      if [[ "$file_name" =~ \.gz$ ]]; then
        gunzip "$file_name"
        unzipped_file="${file_name%.gz}"
      else
        unzipped_file="${file_name}"
      fi
      kraken2-build --db "${db_full_path}" --add-to-library ${unzipped_file}
      echo "fasta_url: ${value}" >>"${info_file}"
      ;;
    ncbi_query)
      echo "Download from NCBI query: ${value}"
      esearch -db nucleotide -query "${value}" | efetch -format fasta > ncbi-query.fasta
      kraken2-build --db "${db_full_path}" --add-to-library ncbi-query.fasta
      echo "ncbi_query: ${value}" >>"${info_file}"
      ;;
    db_zip_url)
      echo "Download from URL: ${value}"
      wget "${value}"

      echo "Uncompress file"
      unzip $(basename "${value}") -d unzipped

      num_subdirectories=$(find unzipped -type d -maxdepth 1 | wc -l)
      if [ "$num_subdirectories" -eq 2 ]; then
        subdirectory=$(find unzipped -mindepth 1 -maxdepth 1 -type d)
        mv "$subdirectory"/* "${db_full_path}"
      else
        mv unzipped/* "${db_full_path}"
      fi
      echo "db_zip_url: ${value}" >>"${info_file}"
      database_built=1
      ;;
    special)
      echo "Build database"
      kraken2-build --db "${db_full_path}" --special ${value}
      database_built=1
      for file in "${db_full_path}/data"/*; do
        if [ -f "$file" ]; then
          echo "data: $(basename "$file")" >>"${info_file}"
        fi
      done
      ;;
    *)
      echo "Unknown key: ${key}"
      ;;
  esac
done < <(grep -vE '^\s*(#|$)' "$1")

if [ "${database_built}" = 0 ]; then
  echo "Build database"
  kraken2-build --db "${db_full_path}" --build
fi

echo "Remove temporary files"
cd ..
rm -r "${db_full_path}/tmp"
