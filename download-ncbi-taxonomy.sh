#!/bin/bash

set -e
source settings.conf

mkdir -p /tmp/temp-kraken-db
kraken2-build --db /tmp/temp-kraken-db --download-taxonomy
new_taxonomy="${ncbi_taxonomies_folder}/ncbi-"`date "+%Y%m%d"`
mv /tmp/temp-kraken-db/taxonomy "${new_taxonomy}"
ln -sf "${new_taxonomy}" "${ncbi_taxonomies_folder}/latest"
echo "Done"
