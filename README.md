# Kraken2-db-manager

*Kraken2-db-manager* is a program to manage [*Kraken 2*](https://github.com/DerrickWood/kraken2) databases. It helps to automate their creation and their update.

## Table of Contents

- [Description](#description)
- [Dependencies](#dependencies)
- [Configuration](#configuration)
- [Taxonomy management](#taxonomy)
- [Databases management](#databases)

## Description

*Kraken2-db-manager* downloads files and build a *Kraken 2* database from the corresponding configuration file. The taxonomy folder can be managed separately if you have multiple databases using the same taxonomy folder.

## Dependencies

You will need to install the following dependencies:
 - *Wget*
 - *Unzip*
 - *Gzip*

On *Ubuntu* or other *Debian*-based distributions:
```
sudo apt update
sudo apt install wget unzip gzip
```

If you're going to create databases from an NCBI query, you will need to install *Entrez* as well:
```
sudo apt install ncbi-entrez-direct
```

## Configuration

The first configuration file you need to fill is the `settings.conf` file. It contains two parameters:
 - `dbs_folder` is the folder containing all of your databases. The new databases will be created in this folder.
 - `ncbi_taxonomies_folder` is the folder containing the *NCBI* taxonomy versions. It is only used if you want to manage the *NCBI* taxonomy separately.

## Taxonomy management
The *NCBI* taxonomy folder needs more than 30GB of disk space. If you have multiple databases using this taxonomy, you can avoid having multiple copies by linking the taxonomy folder to a folder outside the database. The `download-ncbi-taxonomy.sh` script helps you to manage this. Before running it, make sure the `ncbi_taxonomies_folder` setting in `settings.conf` is correct.

It downloads the taxonomy from *NCBI*, puts the file in the taxonomies folder and link the `latest` folder to it. Every database that links its taxonomy to `latest` will have its taxonomy updated. If you encounter some problems with the new taxonomy, you can easily downgrade to the old one by linking `latest` back to it. If the new version is OK, you can remove the old folder.

To download or update the taxonomy, just run the script without arguments:

```
download-ncbi-taxonomy.sh
```

## Databases management

To download a database, run the `build-db.sh` script with the database script-like configuration file:

```
build-db.sh <database-configuration-file>
```

It will download and build the database using the information in the configuration file. A `database.info` file will be added in the database folder with some information about the build. The database script-like configuration file is read and executed line by line. The format of each line is `key=value`. The possible keys are the following:
 - `db_name`: The name of the *Kraken 2* database. You can include the current date in the name using [date format](https://pubs.opengroup.org/onlinepubs/9699919799/utilities/date.html). For example `db-%Y%m%d` will become something like `db-20230728`. This parameter is mandatory and must be the first one of the file.
 - `description`: Optional. It will just add a line with the description in the `database.info` file to help to keep a track of what's in the database.
 - `taxonomy_ln`: Used to link the taxonomy to an existing external folder. If you use the taxonomy management, `taxonomy_ln=$latest` will link this database to the latest taxonomy and it will be kept updated when you update the taxonomy. `taxonomy_ln=/path/to/folder` will link the taxonomy to the given folder.
 - `special`: Used to build a [special database](https://github.com/DerrickWood/kraken2/blob/master/docs/MANUAL.markdown#special-databases). `special=silva` will automatically retrieve the information to download a [*Silva*](https://www.arb-silva.de/) database.
 - `fasta_url`: The URL of a *Fasta* file to be downloaded and added to the database.
 - `ncbi_query`: An *NCBI* query to be executed. The results of the query will be added to the database.
 - `db_zip_url`: If the database is alailable already build, you can give its URL to download and unzip it.

At the end, it will build the database, except if you used a parameter that doesn't require it, like `special` or `db_zip_url`.
